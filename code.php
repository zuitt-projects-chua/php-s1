<?php

// [Section] Comments

/*PHP Comments*/

/*
multi Line,

Echo used to output data to the screen
End every php statement with semicolon
*/

//echo "Mabuhay World";

//[Variables]

$name = 'John Smith';
$email = 'johnsmith@mail.com';

//[Constants]

define('PI', 3.1416);

// define('PI', 4); error
$PI = 4;

//[Data Types]

$state = "New York";
$country = "USA";
$address = $state . ', ' . $country;
$address2 = "$state, $country";
//concatenation

//Integers
$age = 31;
$headcount = 26;

//Floats

$grade = 98.2;
$distance = 1342.12;

//Boolean
$hasTravelledAbroad = true;

//Null
$spouse = null;

//Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => "John Smith",
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'USA'
	]
];

//[Assignment Operators]
$x = 123;
$y = 234;

$isLegalAge = true;
$isRegistered = false;
