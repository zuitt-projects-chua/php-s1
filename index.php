<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP</title>
</head>
<body>

<h1>Hello World</h1>
<p><?php echo "Good day $name! Your email is $email.";?></p>
<p><?php echo PI; ?></p>
<p><?php echo $PI; ?></p>
<p><?php echo $address; ?></p>
<p><?php echo $address2; ?></p>

<h1>Data Types</h1>

<p><?php echo $spouse; ?></p>
<p><?php echo $hasTravelledAbroad; ?></p>
<p><?php echo var_dump($spouse); ?></p>
<p><?php echo gettype($spouse); ?></p>
<p><?php echo gettype($hasTravelledAbroad); ?></p>
<p><?php echo var_dump($hasTravelledAbroad); ?></p>
<p><?php echo print_r($grades); ?></p>
<p><?php echo gettype($grades); ?></p>
<p><?php echo var_dump($grades); ?></p>
<p><?php echo $grades[1];?></p>

<p><?php echo $gradesObj->firstGrading; ?></p>
<p><?php echo $personObj->address->state; ?></p>

<h1>Operators</h1>
<h3>Arithmetic Operators</h3>
<p>Sum: <?php echo $x + $y; ?></p>
<p>Difference: <?php echo $x - $y; ?></p>
<p>Product: <?php echo $x * $y; ?></p>
<p>Quotient: <?php echo $x / $y; ?></p>

<h3>Equality Operators</h3>
<p>Loose Equality: <?php echo var_dump($x == 234); ?></p>
<p>Loose Inequality: <?php echo var_dump($x != 123); ?></p>
<p>Strict Equality: <?php echo var_dump($x === 234); ?></p>
<p>Strict Inequality: <?php echo var_dump($x !== 123); ?></p>

<h3>Greater/Lesser Operators</h3>
<p>Is Lesser: <?php echo var_dump($x < $y ); ?></p>
<p>Is Greater: <?php echo var_dump($x > $y ); ?></p>

<h3>Logical Operators</h3>
<p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
<p>Are All Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
<p>Are All Requirements Met: <?php var_dump(!$isLegalAge && !$isRegistered); ?></p>


</body>
</html>